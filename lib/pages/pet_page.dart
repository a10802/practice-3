import 'package:flutter/material.dart';
import '../model/pet.dart';

class PetPage extends StatefulWidget {
  final Pet pet;
  const PetPage({Key? key, required this.pet}) : super(key: key);

  @override
  State<PetPage> createState() => _PetPageState();
}

class _PetPageState extends State<PetPage> {

  @override
  Widget build(BuildContext context) {    
    final pet = widget.pet;

    return Scaffold(
      appBar: AppBar( title: Text(pet.name) ),
      body: Center(
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(vertical: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                  pet.name,
                  style: const TextStyle(
                    fontSize: 36,
                    color: Colors.blue
                  )
                )
              ),
              Image.asset(pet.image, width: 300),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                  pet.species,
                  style: const TextStyle( fontSize: 24 )
                )
              ),
              Container(
                padding: const EdgeInsets.all(16.0),
                decoration: const BoxDecoration(
                  color: Colors.blue,
                  shape: BoxShape.circle
                ),
                child: Text(
                  pet.age.toString() + ' yr',
                  style: const TextStyle( color: Colors.white, fontSize: 24 )
                )
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text("Regresar")
              )
            ],
          ),
        )
      )
    );
  }
}
