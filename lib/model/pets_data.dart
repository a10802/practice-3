import './pet.dart';

final pets = [
  Pet(
    image: 'images/miel.jpg',
    name: 'Miel',
    species: 'Dog',
    age: 2
  ),
  Pet(
    image: 'images/niebla.jpg',
    name: 'Niebla',
    species: 'Cat',
    age: 1
  ),
  Pet(
    image: 'images/toby.jpg',
    name: 'Toby',
    species: 'Dog',
    age: 10
  ),
  Pet(
    image: 'images/nube.jpg',
    name: 'Nube',
    species: 'Dog',
    age: 1
  ),
];
