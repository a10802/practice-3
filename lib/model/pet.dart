class Pet {
  String image;
  String name;
  String species;
  int age;

  Pet({
    required this.image,
    required this.name,
    required this.species,
    required this.age
  });
}
