import 'package:flutter/material.dart';
import 'dart:math' as math;
import '../model/pet.dart';
import '../pages/pet_page.dart';

class PetWidget extends StatelessWidget {
  final Pet pet;
  const PetWidget({Key? key, required this.pet}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: const Color.fromARGB(77, 192, 192, 192),
      ),
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => PetPage(pet: pet))
          );
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(pet.image, width: 140),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    pet.name,
                    style: const TextStyle(
                      fontSize: 36,
                      fontWeight: FontWeight.normal,
                      color: Colors.blue                    
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(16.0),
                        decoration: BoxDecoration(
                          color: Color( ( math.Random().nextDouble() * 0xFFFFFF ).toInt() ).withOpacity(1.0),
                          shape: BoxShape.circle
                        ),
                        child: Text(
                          pet.age.toString() + ' yr',
                          style: const TextStyle( color: Colors.white, fontSize: 16 ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Text(
                          pet.species,
                          style: const TextStyle( fontSize: 24 )
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      )      
    );
  }
}
